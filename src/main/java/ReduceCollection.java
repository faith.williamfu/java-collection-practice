import java.util.Collections;
import java.util.List;

public class ReduceCollection {
    public static int getMax(List<Integer> list) {
        // Need to be implemented
        // return Collections.max(list);
        return list.stream().reduce(0,(number1, number2) -> number1 > number2 ? number1 : number2);
    }
    
    public static double getAverage(List<Integer> list) {
        // Need to be implemented
        return (double)getSum(list) / list.size();
    }
    
    public static int getSum(List<Integer> list) {
        // Need to be implemented
        return list.stream().reduce(0, Integer::sum);
    }
    
    public static double getMedian(List<Integer> list) {
        // Need to be implemented
        return list.size() % 2 == 0 ? (double)(list.get(list.size() / 2) + list.get(list.size() / 2 - 1)) / 2 : list.get(list.size() / 2);
    }
    
    public static int getFirstEven(List<Integer> list) {
        // Need to be implemented
        return FilterCollection.getAllEvens(list).get(0);
    }
}
