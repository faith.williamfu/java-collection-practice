import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FilterCollection {
    
    public static List<Integer> getAllEvens(List<Integer> list) {
        // Need to be implemented
        return list.stream().filter(number -> number % 2 == 0).collect(Collectors.toList());
    }
    
    public static List<Integer> removeDuplicateElements(List<Integer> list) {
        // Need to be implemented
        return list.stream().distinct().collect(Collectors.toList());
    }
    
    public static List<Integer> getCommonElements(List<Integer> collection1, List<Integer> collection2) {
        // Need to be implemented
        List<Integer> newCollection1 = new ArrayList<>(collection1);
        List<Integer> newCollection2 = new ArrayList<>(collection2);
        newCollection1.retainAll(newCollection2);
        return newCollection1;
    }
}
