import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CollectionOperation {
    
    public static List<Integer> getListByInterval(int left, int right) {
        // Need to be implemented
        List<Integer> list;
        if (left < right) {
            list = IntStream.rangeClosed(left, right).boxed().collect(Collectors.toList());
        } else {
            list = IntStream.rangeClosed(right, left).boxed().collect(Collectors.toList());
            Collections.reverse(list);
        }
        return list;
    }


    public static List<Integer> removeLastElement(List<Integer> list) {
        // Need to be implemented
        List<Integer> newList = new ArrayList<>(list);
        newList.remove(list.size() - 1);
        return newList;
    }
    
    public static List<Integer> sortDesc(List<Integer> list) {
        // Need to be implemented
        Collections.sort(list);
        Collections.reverse(list);
        return list;
    }
    
    
    public static List<Integer> reverseList(List<Integer> list) {
        // Need to be implemented
        Collections.reverse(list);
        return list;
    }
    
    
    public static List<Integer> concat(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        List<Integer> newList1 = new ArrayList<>(list1);
        List<Integer> newList2 = new ArrayList<>(list2);
        newList1.addAll(newList2);
        return newList1;
    }
    
    public static List<Integer> union(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        return concat(list1,list2).stream().distinct().collect(Collectors.toList());
    }
    
    public static boolean isAllElementsEqual(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        List<Integer> newList1 = new ArrayList<>(list1);
        List<Integer> newList2 = new ArrayList<>(list2);
        newList1.removeAll(newList2);
        return newList1.isEmpty();
    }
}
